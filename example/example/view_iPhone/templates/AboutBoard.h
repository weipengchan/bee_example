//
//  AboutBoard.h
//  example
//
//  Created by mac on 14-1-25.
//  Copyright (c) 2014年 geek-zoo studio. All rights reserved.
//

#import "Bee_UIBoard.h"
#import "Bee.h"

@interface AboutBoard : BeeUIBoard
AS_OUTLET( BeeUIScrollView, list2 );
AS_OUTLET(BeeUIButton, ss);
@end
