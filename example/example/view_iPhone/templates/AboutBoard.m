//
//  AboutBoard.m
//  example
//
//  Created by mac on 14-1-25.
//  Copyright (c) 2014年 geek-zoo studio. All rights reserved.
//

#import "AboutBoard.h"
#import "AboutBoardCell_iPhone.h"
#import "AppBoard_iPhone.h"

@interface AboutBoard ()

@end

@implementation AboutBoard
SUPPORT_AUTOMATIC_LAYOUT( YES );
SUPPORT_RESOURCE_LOADING( YES );
DEF_OUTLET(BeeUIScrollView, list2 );

ON_CREATE_VIEWS(signal)
{
    self.view.backgroundColor = SHORT_RGB( 0x333 );
	
	self.navigationBarShown = YES;
	self.navigationBarTitle = @"About";
	self.navigationBarLeft = [UIImage imageNamed:@"menu-button.png"];
    
	self.list2.lineCount = 1;
	self.list2.animationDuration = 0.25f;
	self.list2.baseInsets = bee.ui.config.baseInsets;//关于iOS7适配的
    
	self.list2.whenReloading = ^
	{
        //加载过多数据时先显示第一条的技术
		self.list2.total = 1;
        
		BeeUIScrollItem * item = self.list2.items[0];
		item.clazz = [AboutBoardCell_iPhone class];
	};
}

ON_WILL_APPEAR( signal )
{
	[self.list2 reloadData];
    
	bee.ui.router.view.pannable = YES;
}

ON_WILL_DISAPPEAR( signal )
{
	bee.ui.router.view.pannable = NO;
}

ON_SIGNAL3( BeeUINavigationBar, LEFT_TOUCHED, signal )
{
	[[AppBoard_iPhone sharedInstance] showMenu];
}

@end
